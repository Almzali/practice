import React from "react";

const headerMoon = {
    moon_name : 'Hello Moon'
  }

function Header(props) {
    return(
        <header>
            <h1>{props.data.site_name}</h1>
            
            <Navigator nav = {props.data.nav}/>
            
        </header>
    )
}

function Navigator(props) {
    let data = props.nav;
    const listItem = data.map(item => <li key={item.link}>< a href={item.link}>{item.text}</a></li>)
    return(
    <nav>
        <ul>
            {listItem}
            
           
        </ul>
    </nav>
    )
    

}

// const Nav=()=>{
//     return(
//         <nav>
//         <ul>
//             <li>one</li>
//             <li>two</li>
//             <li>tree</li>
//         </ul>
//     </nav>
//     )
// }
export default Header;