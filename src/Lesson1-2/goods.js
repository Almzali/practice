
function Goods(props) {
    return(
        <div className="good-block">
            <h3>{props.title}</h3>
            <p>{props.const}</p>
            <img src={props.image} alt="{props.title}" />
        </div>
    )
}

export default Goods;
